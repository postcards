module WorkerPool (parMapIO_') where

import Control.Monad (replicateM)
import Control.Concurrent (forkOS)
import Control.Concurrent.STM
  ( TMVar, newEmptyTMVarIO, takeTMVar, putTMVar
  , TBQueue, newTBQueueIO, readTBQueue, writeTBQueue
  , atomically
  )

parMapIO_' :: Int -> Int -> (a -> IO b) -> [a] -> IO ()
parMapIO_' queueSize numWorkers action jobs = do
  bq <- newTBQueueIO queueSize
  ws <- replicateM numWorkers $ do
    w <- newEmptyTMVarIO
    _ <- forkOS $ worker action bq w
    return w
  mapM_ (atomically . writeTBQueue bq) (map Just jobs ++ replicate numWorkers Nothing)
  atomically $ mapM_ takeTMVar ws

worker :: (a -> IO b) -> TBQueue (Maybe a) -> TMVar () -> IO ()
worker action jobs done = loop
  where
    loop = do
      job <- atomically $ readTBQueue jobs
      case job of
        Just j -> action j >> loop  -- FIXME TODO what about exceptions?
        Nothing -> atomically $ putTMVar done ()
